package com.camemax.nacos.multiconfig.controller;

import com.camemax.nacos.multiconfig.pojo.Users;
import com.camemax.nacos.multiconfig.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("multi")
public class NacosMultiConfigController {
    @Autowired
    @Qualifier("usersService")
    private UsersService service;

    @GetMapping("/user/{id}")
    @ResponseBody
    public String getUserInfoById(@PathVariable("id")String id){
        Users user = service.getById(id);
        return user.toString();
    }
}
