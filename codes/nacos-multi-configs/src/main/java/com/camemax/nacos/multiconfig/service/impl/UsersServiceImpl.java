package com.camemax.nacos.multiconfig.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.camemax.nacos.multiconfig.dao.UsersMapper;
import com.camemax.nacos.multiconfig.pojo.Users;
import com.camemax.nacos.multiconfig.service.UsersService;
import org.springframework.stereotype.Service;

@Service("usersService")
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {

}
