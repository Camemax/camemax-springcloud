package com.camemax.nacos.multiconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NacosMultiConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosMultiConfigApplication.class,args);
    }
}
