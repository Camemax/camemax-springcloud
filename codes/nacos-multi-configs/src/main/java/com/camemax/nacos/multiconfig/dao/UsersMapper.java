package com.camemax.nacos.multiconfig.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.camemax.nacos.multiconfig.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UsersMapper extends BaseMapper<Users> {
}
