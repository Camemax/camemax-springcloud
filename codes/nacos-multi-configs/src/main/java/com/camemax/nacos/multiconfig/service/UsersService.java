package com.camemax.nacos.multiconfig.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.camemax.nacos.multiconfig.pojo.Users;

public interface UsersService extends IService<Users> {

}
