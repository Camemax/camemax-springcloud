package com.camemax.client.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

// *********************
// ** @Date: 2021-06-28
// ** @Author: Camemax
// ** @Description: OpenFeign使用步骤： ①声明微服务名称的接】 【②启动项指定调用接口的所在路径】 ③控制层调用接口，实现Feign远程调用。
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients( basePackages = "com.camemax.client.openfeign.feign")
public class OpenFeignClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignClientApplication.class, args);
    }
}
