package com.camemax.client.openfeign.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
// *********************
// ** @Date: 2021-06-28
// ** @Author: Camemax
// ** @Description: OpenFeign使用步骤： 【①声明微服务名称的接口】 ②启动项指定调用接口的所在路径 ③控制层调用接口，实现Feign远程调用
// *********************
@FeignClient("openfeign-server") // 指定微服务名称
public interface OpenFeignClient {
    @RequestMapping("/opserver/test")
    String openfeignServer(); // 返回类型与微服务中的对应方法要一致，否则报UnknownContentTypeException异常。
}
