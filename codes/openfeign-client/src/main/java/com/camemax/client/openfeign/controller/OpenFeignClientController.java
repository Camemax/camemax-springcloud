package com.camemax.client.openfeign.controller;

import com.camemax.client.openfeign.feign.OpenFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
// *********************
// ** @Date: 2021-06-28
// ** @Author: Camemax
// ** @Description: OpenFeign使用步骤： ①声明微服务名称的接口 ②启动项指定调用接口的所在路径 【③控制层调用接口，实现Feign远程调用】
// *********************
@Controller
@RequestMapping("opclient")
public class OpenFeignClientController {
    // 加载声明式接口
    @Autowired
    private OpenFeignClient client;

    @ResponseBody
    @GetMapping("/test")
    public String clientAskServer(){
        return client.openfeignServer();
    }
}
