package com.camemax.nacos.config.controller;

import com.camemax.nacos.config.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// *********************
// ** @Date: 2021-06-28
// ** @Author: Camemax
// ** @Description: nacos配置中心请求，验证nacos热部署配置是否生效
// *********************

@RestController
@RequestMapping("config")
public class UsersController {

    @Autowired
    @Qualifier("users")
    Users user;

    @GetMapping("/test")
    public String testConfig(){
        return user.toString();
    }
}
