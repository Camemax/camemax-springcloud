package com.camemax.nacos.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient // nacos开启服务发现
public class NacosConfigurationApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigurationApplication.class,args);
    }
}
