package com.camemax.server.openfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class OpenFeignServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(OpenFeignServerApplication.class,args);
    }
}
