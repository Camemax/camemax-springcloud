package com.camemax.server.openfeign.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("opserver")
public class OpenFeignServerController {
    @ResponseBody
    @GetMapping("/test")
    public String openfeignServer(){
        return "Openfeign-Server accepts request!";
    }
}
