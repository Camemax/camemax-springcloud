package com.camemax.nacos_config.settings.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

// *********************
// ** @Date: 2021-06-28
// ** @Author: Camemax
// ** @Description: nacos配置中心实体类 => 用于直观测试配置中心生效结果
// *********************

@Repository("users")
@RefreshScope
@ConfigurationProperties( prefix = "pojo")
public class Users {
    @Value("${pojo.username}")
    private String username;
    @Value("${pojo.email}")
    private String email;

    public Users() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Users{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
