package com.camemax.nacos_config.settings.controller;

import com.camemax.nacos_config.settings.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("cs") // config settings
public class NacosConfigSettingsController {

    @Autowired
    private Users user;

    @ResponseBody
    @GetMapping("/test")
    public String test(){
        return user.toString();
    }
}
