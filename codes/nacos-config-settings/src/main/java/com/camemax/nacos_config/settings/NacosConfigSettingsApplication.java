package com.camemax.nacos_config.settings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NacosConfigSettingsApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigSettingsApplication.class,args);
    }
}
