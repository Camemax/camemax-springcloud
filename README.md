# camemax-springcloud

#### 介绍
循序渐进的学习与测试Spring-Cloud-Alibaba

#### 内容说明

Including some Modules.<br>
- **alibaba-cloud-root**: root module.<br/>
- **alibaba-cloud-common-dependencies**: import the same dependencies, spring-cloud-dependencies & spring-cloud-alibaba-dependencies.<br/>
- **nacos-discovery-server**: study the basic use for nacos discovery.<br/>
- **nacos-config-server**: study the basic use for nacos config.<br/>
- **openfeign-server** & **nacos-discovery-server**: study the basic use for OpenFeign.<br/>
- **nacos-config-settings**: study the basic use of data Id、group、namespace.<br/>
<p>All of those are used to study the basic use of Nacos、OpenFeign(temporary, could be more in future).

#### 使用目的
1.  **[for studying to test]** A minimum level of functionality for **Nacos Discovery**.<br/>
2.  **[for studying to test]** A minimum level of functionality for **Nacos Config**.<br/>
3.  **[something else about SpringCloud]** A minimum level of functionality for **OpenFeign**.<br/>